#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>


#include "IDVarTest/Test.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];
  
  // Decide how many events to run over:
  int nEvents = 100;

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;


  // use SampleHandler to scan all of the subdirectories of a directory 
  const char* inputFilePath = gSystem->ExpandPathName ("/ZIH.fast/users/kirchmeier/xAOD/user.dkirchme.mc15_13TeV.303367.RS_G_hh_bbtt_hh_c10_M2000.recon.ESD.e4438_s2608_r6869_v01_EXT0.56786618/");
  // SH::ScanDir().filePattern("user.dkirchme.*.pool.root").scan(sh, inputFilePath);
  SH::ScanDir().filePattern("user.dkirchme.7168981.EXT0._000007.pool.root").scan(sh, inputFilePath);

  // set the name of the tree in input files
  sh.setMetaString ("nc_tree", "CollectionTree");

  // print out the samples found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh);
  job.options()->setDouble (EL::Job::optMaxEvents, nEvents);

  // add our algorithm to the job
  Test *alg = new Test;

  job.algsAdd (alg);

  // define driver
  EL::DirectDriver driver;

  // process the job using the driver
  driver.submit (job, submitDir);
}
