#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <IDVarTest/Test.h>

// EDM includes:
#include "xAODTau/DiTauJetContainer.h"

// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "EventLoop/OutputStream.h"


typedef std::vector< ElementLink< xAOD::TrackParticleContainer > >  TrackParticleLinks_t;

// this is needed to distribute the algorithm to the workers
ClassImp(Test)

/// Helper macros for checking and retriving xAOD containers
#define CHECKTOOL( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "calcDiTauVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define CHECK( ARG )                    \
  do {                                                  \
    const EL::StatusCode result = ARG;                \
    if( result == EL::StatusCode::FAILURE ) {                    \
      ::Error( "IDVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define EL_RETURN_CHECK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
      return EL::StatusCode::FAILURE;               \
    }  \
  } while( false )





Test :: Test ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode Test :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());  // call before opening first file

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kClassAccess; // alternative: xAOD::TEvent::kBranchAccess;
  xAOD::TEvent event(mode);

  EL::OutputStream out("outputLabel", "xAOD");
  job.outputAdd(out);

  return EL::StatusCode::SUCCESS;

}



EL::StatusCode Test :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  xAOD::TEvent* event = wk()->xaodEvent();

  // write to output xAOD
  TFile *file = wk()->getOutputFile("outputLabel");
  EL_RETURN_CHECK("initialize", event->writeTo(file));

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  xAOD::TEvent* event = wk()->xaodEvent();

  EL_RETURN_CHECK("execute()", event->copy("DiTauJets"));
  EL_RETURN_CHECK("execute()", event->copy("TruthParticles"));
  EL_RETURN_CHECK("execute()", event->copy("TauJets"));
  // EL_RETURN_CHECK("execute()", event->copy("InDetTrackParticles"));


    // get ditau container
  const xAOD::DiTauJetContainer* xDiTauContainer = 0;
  EL_RETURN_CHECK("execute()", event->retrieve(xDiTauContainer, "DiTauJets"));

  // loop over ditau candidates
  for (const auto xDiTau: *xDiTauContainer) 
  {
    // check track links
    if (!xDiTau->isAvailable< TrackParticleLinks_t >("trackLinks") )
    {
      Warning("execute()", "Link not available");
    }

    // check if track links are valid 
    const TrackParticleLinks_t links = xDiTau->auxdata<TrackParticleLinks_t>("trackLinks");
    for (auto link : links)
    {
      if (!link.isValid())
      {
        Warning("execute()", "Link not valid");
      }    
    }  

    TrackParticleLinks_t xTracks = xDiTau->trackLinks();
    Info("execute()", "loop track links");
    Info("execute()", "nTracks: %lu", xTracks.size());
    for (auto xTrack: xTracks) 
    { 
      Info("execute()", "track pt: %f", (*xTrack)->pt());
    }
  }

  event->fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode Test :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
